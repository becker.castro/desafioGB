variable "aws_region" {
  default = "us-east-1"
}

variable "bucket_region" {
  default = "us-east-1"
}

variable "bucket_name" {
  default = "logs-v1-dev"
}

variable "zip_file" {
  default = "logs.zip"
}
