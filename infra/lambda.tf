data "aws_caller_identity" "current" {}

output "account_id" {
  value = "${data.aws_caller_identity.current.account_id}"
}

# LAMBDA
resource "aws_lambda_function" "lambdav1" {
  s3_bucket     = var.bucket_name
  s3_key        = var.zip_file
  function_name = "lambda"
  role          = aws_iam_role.lambda_role.arn
  handler       = "index.handler"
  runtime       = "nodejs12.x"
  timeout       = 60
  memory_size   = 256
}


# ROLE AND POLICY
resource "aws_iam_role" "lambda_role" {
  name = "lambda_s3_tf"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "policy" {
  name = "lambda_s3_access_tf"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "kms:Decrypt",
                "s3:ListBucket",
                "s3:ListMultipartUploadParts",
                "kms:ReEncrypt*",
                "kms:GenerateDataKey*",
                "s3:PutObject",
                "s3:GetObject",
                "s3:AbortMultipartUpload",
                "kms:Encrypt",
                "s3:GetObjectTagging",
                "kms:DescribeKey",
                "s3:PutObjectTagging",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::logs-v1-dev/*",
                "arn:aws:s3:::logs-v1-dev"
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "lambda:ListFunctions",
                "lambda:InvokeFunction",
                "lambda:ListVersionsByFunction",
                "lambda:GetFunction",
                "lambda:ListAliases",
                "s3:ListBucket",
                "lambda:InvokeAsync",
                "s3:ListMultipartUploadParts",
                "s3:PutObject",
                "s3:GetObject",
                "s3:AbortMultipartUpload",
                "lambda:ListEventSourceMappings",
                "s3:GetObjectTagging",
                "s3:PutObjectTagging",
                "lambda:ListLayerVersions",
                "lambda:ListLayers",
                "s3:DeleteObject",
                "s3:HeadBucket",
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_attach" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.policy.arn
}

# API GATEWAY
resource "aws_api_gateway_rest_api" "mydemoapi" {
  name = "myapi"
  description = "This is my API for purposes"
}

resource "aws_api_gateway_resource" "resource" {
  path_part   = "resource"
  parent_id   = aws_api_gateway_rest_api.mydemoapi.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.mydemoapi.id
}

resource "aws_api_gateway_method" "method" {
  rest_api_id   = aws_api_gateway_rest_api.mydemoapi.id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = aws_api_gateway_rest_api.mydemoapi.id
  resource_id             = aws_api_gateway_resource.resource.id
  http_method             = "ANY"
  integration_http_method = "ANY"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambdav1.invoke_arn
}


# LAMBDA PERMISSION
resource "aws_lambda_permission" "apigw-lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambdav1.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "arn:aws:execute-api:${var.aws_region}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.mydemoapi.id}/*/${aws_api_gateway_method.method.http_method}${aws_api_gateway_resource.resource.path}"
}
