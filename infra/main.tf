provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}

# terraform {
#   backend "s3" {
#     encrypt = true
#     bucket = "terraform-cb"
#     key    = "dev.tfstate"
#     region = "eu-west-1"
#   }
# }
