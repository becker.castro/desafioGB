# Tarefas

### Pré-requisitos 

- Bucket S3 (logs-v1-dev) onde o código do lambda será salvo, o mesmo deverá ter o formato zip.
- AWS Account - Access key e secret key, as chaves devem ser preenchidas nas variáveis do gitlab conforme imagem abaixo:

(https://gitlab.com/becker.castro/desafioGB/blob/master/images/ScreenShot.png)

**Build**

Ao executar um commit no repo, damos inicío ao processo da pipeline, basicamente este stage irá preparar o ambiente para a criação do lambda, coletando o arquivo .zip contendo o código de sua função, onde no próximo stage o mesmo será utilizado.

**Lambda Create**

Neste stage o mesmo criará a infraestrutura completa, contendo função lambda utilizando o zip citado no stage anterior e api gateway.

Para o acesso a app, o endereço será disponibilizado no painel da aws, tópico API Gateway por segurança e boas práticas.

**Logs**

Os logs relacionados a app, são enviados para o CloudWatch Logs, onde os mesmos possuem persintencia e podem ser exportados para um bucket S3.


**CI/CD**

Nesta sessão estou utilizando o serviço do gitlab, onde os stages são executados através dos runners em container, descritos no arquivo ``gitlab-ci.yml``.

**Arquitetura**


A arquiterura é composta por um API Gateway e Lambda, o ApiGtw está sendo utilizado para disponibilizar uma URL de acesso a app, onde a aplicação é apenas um hello world simples e salva seus logs utilizando o cloudwatch logs, gerando assim uma melhor estabilidade e menor custo para o ambiente 

```mermaid
graph LR
A[URL ApiGtw] -- request -->B((Lambda))
B -- logs --> C{CloudWatch Logs}
```

